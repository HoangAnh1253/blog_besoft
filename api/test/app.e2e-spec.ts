import { createTestingApp } from './common/test-setup';
import { Sdk } from './gql/queries';

describe('AppController (e2e)', () => {
  let session: Sdk;
  beforeEach(async () => {
    const { sessionFactory } = await createTestingApp();
    session = await sessionFactory.create();
  });

  it('should create a user', async () => {
    const { users } = await session.listUsers();
    console.log(users);
    expect(users).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          userName: 'anhanh123',
          phoneNumber: '123654789',
        }),
      ]),
    );
  });//adasd
});
