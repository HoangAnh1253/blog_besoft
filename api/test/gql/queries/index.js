import fs from 'fs';
import path from 'path';

module.exports.isWorking = fs.readFileSync(path.join(__dirname, 'isWorking.gql'), 'utf8');
module.exports.listUsers = fs.readFileSync(path.join(__dirname, 'listUsers.gql'), 'utf8');
