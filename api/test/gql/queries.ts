import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Mutation = {
  __typename?: 'Mutation';
  createUser: Scalars['String'];
  deleteUser: Scalars['String'];
  updateUser: Scalars['String'];
};


export type MutationCreateUserArgs = {
  input: UserObjectInput;
};


export type MutationDeleteUserArgs = {
  userId: Scalars['String'];
};


export type MutationUpdateUserArgs = {
  input: UpdateUserObjectInput;
  userId: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  isWorking: Scalars['Boolean'];
  listUsers: Array<UserObject>;
};

export type UpdateUserObjectInput = {
  location?: InputMaybe<UserLocationObjectInput>;
  name?: InputMaybe<Scalars['String']>;
};

export type UserLocationObject = {
  __typename?: 'UserLocationObject';
  city?: Maybe<Scalars['String']>;
  country: Scalars['String'];
};

export type UserLocationObjectInput = {
  city?: InputMaybe<Scalars['String']>;
  country: Scalars['String'];
};

export type UserObject = {
  __typename?: 'UserObject';
  phoneNumber: Scalars['String'];
  userName: Scalars['String'];
};

export type UserObjectInput = {
  location: UserLocationObjectInput;
  name: Scalars['String'];
};

export type CreateUserMutationVariables = Exact<{
  input: UserObjectInput;
}>;


export type CreateUserMutation = { __typename?: 'Mutation', createUser: string };

export type DeleteUserMutationVariables = Exact<{
  userId: Scalars['String'];
}>;


export type DeleteUserMutation = { __typename?: 'Mutation', deleteUser: string };

export type UpdateUserMutationVariables = Exact<{
  userId: Scalars['String'];
  input: UpdateUserObjectInput;
}>;


export type UpdateUserMutation = { __typename?: 'Mutation', updateUser: string };

export type IsWorkingQueryVariables = Exact<{ [key: string]: never; }>;


export type IsWorkingQuery = { __typename?: 'Query', isWorking: boolean };

export type ListUsersQueryVariables = Exact<{ [key: string]: never; }>;


export type ListUsersQuery = { __typename?: 'Query', users: Array<{
    userName,
    phoneNumber
  }> };


export const CreateUserDocument = gql`
    mutation createUser($input: UserObjectInput!) {
  createUser(input: $input)
}
    `;
export const DeleteUserDocument = gql`
    mutation deleteUser($userId: String!) {
  deleteUser(userId: $userId)
}
    `;
export const UpdateUserDocument = gql`
    mutation updateUser($userId: String!, $input: UpdateUserObjectInput!) {
  updateUser(userId: $userId, input: $input)
}
    `;
export const IsWorkingDocument = gql`
    query isWorking {
  isWorking
}
    `;
export const ListUsersDocument = gql`
    query listUsers {
      users{
    userName,
    phoneNumber
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: (requestHeaders?:Record<string, string>) => Promise<T>, operationName: string, operationType?: string) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = (action, _operationName, _operationType) => action();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    createUser(variables: CreateUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<CreateUserMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<CreateUserMutation>(CreateUserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'createUser', 'mutation');
    },
    deleteUser(variables: DeleteUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteUserMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<DeleteUserMutation>(DeleteUserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'deleteUser', 'mutation');
    },
    updateUser(variables: UpdateUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateUserMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UpdateUserMutation>(UpdateUserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'updateUser', 'mutation');
    },
    isWorking(variables?: IsWorkingQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<IsWorkingQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<IsWorkingQuery>(IsWorkingDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'isWorking', 'query');
    },
    listUsers(variables?: ListUsersQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListUsersQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<ListUsersQuery>(ListUsersDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'liádasdasdstUsers', 'query');
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;