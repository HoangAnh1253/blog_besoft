import { ObjectType, Field, Int, ID } from '@nestjs/graphql';

@ObjectType()
export class File {
  @Field(() => String, { description: 'Example field (placeholder)' })
  url: string;
}
