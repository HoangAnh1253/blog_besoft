import { CreateUserInput } from './create-user.input';
import { InputType, Field, ID, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateUserInput extends PartialType(CreateUserInput) {
  @Field(() => String, { description: 'Example field (placeholder)' })
  userName: string;

  @Field(() => String, { nullable: true })
  password?: string;

  @Field(() => String, { description: 'Example field (placeholder)' })
  email?: string;

  @Field(() => String, { description: 'Example field (placeholder)' })
  phoneNumber?: string;
}
