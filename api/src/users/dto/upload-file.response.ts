import { CreateUserInput } from './create-user.input';
import { InputType, Field, ID, PartialType } from '@nestjs/graphql';

@InputType()
export class UploadFileResponse extends PartialType(CreateUserInput) {
  @Field(() => Boolean, { description: 'Example field (placeholder)' })
  success: boolean;
}
