import { Injectable } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User, UserDocument } from './user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(createUserInput: CreateUserInput): Promise<User> {
    const password = await bcrypt.hash(createUserInput.password, 2);
    const newUser = { ...createUserInput, password };
    const createdUser = new this.userModel(newUser);
    return await createdUser.save();
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find();
  }

  async findOne(userName: string): Promise<User> {
    return await this.userModel.findOne({ userName });
  }

  async update(
    username: string,
    updateUserInput: UpdateUserInput,
  ): Promise<User> {
    const { userName, ...updateInfo } = updateUserInput;
    return await this.userModel.findOneAndUpdate(
      { userName },
      { ...updateInfo },
      { new: true },
    );
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
