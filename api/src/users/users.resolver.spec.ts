import { Test, TestingModule } from '@nestjs/testing';
import { UsersResolver } from './users.resolver';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

describe('UsersResolver', () => {
  const dto = [
    {
      _id: '1',
      userName: 'anh',
      password: 'anh',
      email: 'anh',
      phoneNumber: '123456789',
    },
  ];
  let usersResolver: UsersResolver;
  const mockJwtAuth = jest.fn(() => true);
  const mockUserService = {
    findAll: jest.fn(() => {
      return dto;
    }),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersResolver,
        { provide: UsersService, useValue: mockUserService },
        { provide: JwtAuthGuard, useValue: mockJwtAuth },
      ],
    }).compile();

    usersResolver = module.get<UsersResolver>(UsersResolver);
  });

  it('should be defined', () => {
    expect(usersResolver.findAll()).toEqual(dto);
  });
});
