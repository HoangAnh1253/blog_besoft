import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { User, UserSchema } from './user.schema';
import mockingoose from 'mockingoose';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
describe('UsersService', () => {
  let service: UsersService;
  const userModel = {
    find: jest.fn(() => {
      return 'anh';
    }),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getModelToken(User.name),
          useValue: userModel,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service.findAll()).toEqual({});
  });
});
