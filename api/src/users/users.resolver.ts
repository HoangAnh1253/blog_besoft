import { Resolver, Query, Mutation, Args, Int, Context } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { UseGuards, UnauthorizedException } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { GraphQLUpload, FileUpload } from 'graphql-upload';
import * as fs from 'fs';

@Resolver(() => User)
export class UsersResolver {
  constructor(private usersService: UsersService) {}

  @Mutation(() => User)
  async createUser(@Args('createUserInput') createUserInput: CreateUserInput) {
    const user = await this.usersService.findOne(createUserInput.userName);
    if (user) {
      throw new Error('User already exist!');
    }
    return this.usersService.create(createUserInput);
  }

  @Query(() => [User], { name: 'users' })
  findAll() {
    return this.usersService.findAll();
  }

  @Query(() => String)
  getString(str: string) {
    return str;
  }

  @Query(() => User, { name: 'user' })
  @UseGuards(JwtAuthGuard)
  findOne(
    @Context() context,
    @Args('userName', { type: () => String }) userName: string,
  ) {
    if (context.req.user.username !== userName) {
      console.log(context.req.user.username, userName);
      throw new UnauthorizedException();
    }
    return this.usersService.findOne(userName);
  }

  @Mutation(() => Boolean)
  async uploadFile(
    @Args({ name: 'file', type: () => GraphQLUpload })
    { createReadStream, filename }: FileUpload,
  ) {
    const stream = createReadStream();
    const path = `./public/images/${filename}`;
    const writeStream = fs.createWriteStream(path);
    await stream.pipe(writeStream);
    return true;
  }

  @Mutation(() => User)
  @UseGuards(JwtAuthGuard)
  updateUser(
    @Context() context,
    @Args('updateUserInput') updateUserInputt: UpdateUserInput,
  ) {
    if (context.req.user.username !== updateUserInputt.userName) {
      throw new UnauthorizedException();
    }
    return this.usersService.update(
      updateUserInputt.userName,
      updateUserInputt,
    );
  }

  @Mutation(() => User)
  removeUser(@Args('id', { type: () => Int }) id: number) {
    return this.usersService.remove(id);
  }
}
