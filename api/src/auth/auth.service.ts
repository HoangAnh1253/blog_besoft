import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { LoginUserInput } from './dto/login-user.input';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(userName: string, password: string): Promise<any> {
    const user = await this.usersService.findOne(userName);
    const validatePassword = await bcrypt.compare(password, user?.password);
    if (user && validatePassword) {
      //const { password, ...result } = user;
      return user;
    }
    return null;
  }

  async login(loginUserInput: LoginUserInput) {
    const { username, password } = loginUserInput;
    const user = await this.usersService.findOne(username);
    return {
      access_token: this.jwtService.sign({
        username: user.userName,
        sub: user._id,
      }),
      user,
    };
  }
}
